declare type NextMiddleware = (arg0: Error | void) => void;

declare type Middleware = (arg0: TRequest, arg1: TResponse, arg2: NextMiddleware) => mixed | void;

declare type AsyncMiddleware = (arg0: TRequest, arg1: TResponse, arg2: NextMiddleware) => Promise<mixed | void>;

declare type ErrorHandler<E, N> = (E, N) => void

declare type MaybePromise = () => Promise<mixed>;

declare type ValidResp = [void, mixed];

declare type InvalidResp = [typeof Error, void];
