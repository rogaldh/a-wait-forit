"use strict";

const nextMiddleware = (error: Error, next: NextMiddleware) => {
  next(error);
};
export const wait =
  (
    promise: AsyncMiddleware,
    onerror: ErrorHandler<Error, NextMiddleware> | void,
    inext: number = 2
  ): Middleware =>
  (...argv) => {
    const next = argv[inext];
    // expect middleware's `next` by default

    if (onerror && typeof onerror !== "function")
      throw new TypeError("`onerror` has wrong type");
    return promise(...argv).catch((error: Error) => {
      return onerror ? onerror(error, next) : nextMiddleware(error, next);
    });
  };
export const forit = (
  maybePromise: MaybePromise,
  onerror: Function | void
): Promise<ValidResp | InvalidResp> => {
  const onError = onerror || ((_: typeof Error) => [_, undefined]);
  return (typeof maybePromise === "function" ? maybePromise() : maybePromise)
    .then((_) => [undefined, _])
    .catch((error: typeof Error) => onError(error));
};
